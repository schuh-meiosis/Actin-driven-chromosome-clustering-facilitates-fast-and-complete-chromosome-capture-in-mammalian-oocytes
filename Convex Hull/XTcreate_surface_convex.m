%  create a convex surface from  a surface object 
%
%
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory.
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%        <Item name="create a convex surface from a surface object" icon="Matlab" tooltip="create a convex surface">
%          <Command>MatlabXT::XTcreate_surface_convex(%i)</Command>
%        </Item>
%      </Menu>
%    </CustomTools>
%  
%
%  Description:
%   only uses the largest surface at each timepoint
%
function XTcreate_surface_convex(aImarisApplicationID)

    if isempty(cell2mat(strfind(javaclasspath('-all'), 'ImarisLib.jar')))
          mlock
          javaaddpath ImarisLib.jar
          munlock
    end

    % connect to Imaris interface
    if ~isa(aImarisApplicationID, 'Imaris.IApplicationPrxHelper')
      vImarisLib = ImarisLib;
      if ischar(aImarisApplicationID)
        aImarisApplicationID = round(str2double(aImarisApplicationID));
      end
      imaris = vImarisLib.GetApplication(aImarisApplicationID);
    else
      imaris = aImarisApplicationID;
    end

    %only works in Imaris 9 for surface creation of ellipsoid
    imaris_version_date = get_imaris_version(imaris);
    if(imaris_version_date > datetime(2017,7,15)) %after Imaris version 9, surfaces are handled differently
        errordlg('Imaris 8.4.2 or older required (yes older)')
    end
    
    
    factory = imaris.GetFactory;
    scene = imaris.GetSurpassScene;
    number_children = scene.GetNumberOfChildren;
    children_names_list = cell(number_children,1);
    for i=0:number_children-1
        if(~isempty(scene.GetChild(i)))
            children_names_list{i+1} =  char(scene.GetChild(i).GetName);
        else
            children_names_list{i+1} = ' ';
        end
    end
    surfaceID = listdlg('PromptString',{'Select the surface object';'object in Imaris:'},...
            'SelectionMode','single',...
            'ListString',children_names_list);
        
    surface = factory.ToSurfaces(scene.GetChild(surfaceID-1));
    
    if(isempty(surface))
        errordlg('No surface selected');
    end
    
    volume = get_statistic(surface,'Volume');
    
    times = get_statistic(surface,'Time Index');
    
    center_x = get_statistic(surface,'Position X');
    center_y = get_statistic(surface,'Position Y');
    center_z = get_statistic(surface,'Position Z');
    
    time_max = max(times);
    time_min = min(times);
    
    surface_convex = factory.CreateSurfaces;
    for time=time_min:time_max
        ids_now = times == time;
        [~,id_largest_now] = max(volume .* ids_now);
        
        vertices = surface.GetVertices(id_largest_now-1);
        triangles = convhulln(double(vertices));
        
        ids_vertices_used = unique(triangles(:));
        vertices_new = vertices(ids_vertices_used,:);
        ids_new = zeros(size(vertices,1),1);
        ids_new(ids_vertices_used) = 1:numel(ids_vertices_used);
        triangles_new = ids_new(triangles);
        triangles_new = triangles_new(:,[1 3 2])-1;
        
        normals_new = vertices_new - [center_y(id_largest_now) center_x(id_largest_now) center_z(id_largest_now)]; % normals from the approximate center of the surface
        surface_convex.AddSurface(vertices_new, int32(triangles_new), normals_new, int32(time-1));
    end
    
    scene.AddChild(surface_convex,-1);
    disp('1')
    

end
