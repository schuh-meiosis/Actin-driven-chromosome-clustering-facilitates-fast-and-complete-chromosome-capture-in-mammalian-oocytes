
# Convex Surface

---

##Overview:
	This script computes the convex hull from Imaris surface objects and creates a new surface object accordingly.
	
---
	
##Installation
	No installation required, just copy the folder to an accessible location and navigate to it in Matlab. All required functions are eithere there or in the subfolder "private"
	
---

##Requirements
### Matlab
		Tested on Matlab R2018b in Windows 10

###	Imaris
		Imaris 8.4.1 or older, Imaris 9+ is not supported
		XTensions module

---

##Description
	Before starting the script, you need to create a surface object in Imaris.
	The script will take the largest segment of the surface object per frame and create a convex surface of it.
	The script uses the Matlab function convhulln to compute the convex hull from the vertices defining the surface.
	
	

		
## Usage
	1. Create a surface in Imaris
	run in Matlab:
	2. imaris = GetImaris_pathed("path to ImarisLib.jar in imaris folder")
	3. XTcreate_surface_convex(imaris)
	

###		Tipps:
	If you want to use all segments of the surface object, you can unify them before using the script in the edit tab.
	Switching to the filter tab and back to the edit tab should select all surface segments across all frames and allow easy unification if desired.
	batch_surfaces_convex(imaris) runs the script on all imaris files within the chosen folder		
---

## Troubleshooting:
	reading and writing statistics in Imaris requires a lot of java heap memory. You can increase this in MATLABs "Preferences" under "MATLAB/General/Java Heap Memory"
	
	The resulting surface may look odd, this is likely due to some issue with the surface normals. No serious problems have been found with this though, it appears to be purely visual.
	
	When creating the object "imaris" in step 2, the ImarisLib.jar file has to be the correct one. If for example Imaris 9.0 and 8.4 are installed, the file from the Imaris 8.4 installation folder must be used. Otherwise, different types of errors can occur.
	
	Randomly, the communication between Imaris and Matlab can fail with random errors. In these cases Imaris and Matlab must be completely restarted.
---
	
##Contact

Eike-Urs.Moennich@mpinat.mpg.de
	
---

##References:
