%   does not support Imaris 9 after rework of surfaces
%  compute max distance between two points on "surface" of a surface object
%
%
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory.
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%        <Item name="compute max distance on a surface object" icon="Matlab" tooltip="compute max distance on a surface object">
%          <Command>MatlabXT::XTmax_distance_on_surface(%i)</Command>
%        </Item>
%      </Menu>
%    </CustomTools>
%  
%
%  Description:
%   
%   determines the largest distance between any pair of vertices defining
%   the individual surfaces and writes them into the statistics
%

function distance_max = XTmax_distance_on_surface(aImarisApplicationID)
    % connect to Imaris interface
    if ~isa(aImarisApplicationID, 'Imaris.IApplicationPrxHelper')
      javaaddpath ImarisLib.jar
      vImarisLib = ImarisLib;
      if ischar(aImarisApplicationID)
        aImarisApplicationID = round(str2double(aImarisApplicationID));
      end
      imaris = vImarisLib.GetApplication(aImarisApplicationID);
    else
      imaris = aImarisApplicationID;
    end
    
    imaris_date_time = get_imaris_version(imaris);
    if(imaris_date_time > datetime(2017,7,15))
        warning('Imaris Version 8.4 required for surface analysis')
        distance_max =-1;
        return;
    end
    factory = imaris.GetFactory;
    surface_object = imaris.GetSurpassSelection;
    surface_object = factory.ToSurfaces(surface_object);
    if isempty(surface_object)
        errordlg('No surface object selected in Imaris')
    end
    spots_distant = factory.CreateSpots;
    spot_vertices = [];
    spot_times = [];
    ids = 0:(surface_object.GetNumberOfSurfaces()-1);
    distance_max = -1*ones(numel(ids),1);
    h = waitbar(0,['Computing distance for surface 1 out of ' num2str(numel(ids))]);
    for i = 1:numel(ids)
        waitbar(i/numel(ids),h,['Computing distance for surface ' num2str(i) ' out of ' num2str(numel(ids))]);
        id = ids(i);
        vertices = surface_object.GetVertices(id);
        num_vertices = size(vertices,1);
        if num_vertices>100000
            warndlg('More than 100k vertices found, skipping surface!','Size Warning','replace')
            continue
        end
        distances = pdist(vertices);
        [distance_max(i), id_max] = max(distances(:));
        [id_1,id_2] = get_ids(id_max,num_vertices);
        spot_vertices = [spot_vertices ; vertices(id_1,:) ; vertices(id_2,:)];
        spot_times = [spot_times; surface_object.GetTimeIndex(id); surface_object.GetTimeIndex(id)];
    end
    delete(h)
    add_spots(spots_distant,spot_vertices,spot_times);
    imaris.GetSurpassScene.AddChild(spots_distant,-1);
    add_to_stats(surface_object,distance_max);
end

function add_to_stats(object,values)

%     ids = surface_object.GetIds();
    ids_0 = 0:(object.GetNumberOfSurfaces()-1);
    ids = object.GetIds();
    number_surfaces = numel(ids);
    aNames = cell((number_surfaces), 1); 
    aNames(:) = {'maximum inner distance'};
    aValues = values;
    vUnits    = cell(number_surfaces, 1);
    vUnits(:) = { 'NA' };
    FactorNames = {'Category';'Channel';'Time'};
    %initialize Factors
    vFactors = repmat({'a';'b';'c'},[1,number_surfaces]);
    for j=1:number_surfaces
        vFactors(:,j) = {'Surface';'1';num2str(object.GetTimeIndex(ids_0(j))+1) };
    end
    
    object.AddStatistics(aNames, aValues, ...
    vUnits, vFactors, FactorNames, ids);
end

function check_vertices(vertices)
%     if size(vertices,1)>100000
        
%         answer = questdlg('More than 100k vertices found, skip or ?', ...
%             'Dessert Menu', ...
%             'Ice cream','Cake','No thank you','No thank you');
%         % Handle response
%         switch answer
%             case 'Ice cream'
%                 disp([answer ' coming right up.'])
%                 dessert = 1;
%             case 'Cake'
%                 disp([answer ' coming right up.'])
%                 dessert = 2;
%             case 'No thank you'
%                 disp('I''ll bring you your check.')
%                 dessert = 0;
%         end
%     end
end

function [id_1,id_2] = get_ids(x,m)
    el_in_row =0;
    for row = 1:m
        el_in_row = el_in_row + m-row;
        if x < el_in_row
            id_2 = row;
            id_1_temp = el_in_row - x;
            break
        end
    end
    id_1 = m - id_1_temp;
    
end

function add_spots(spots_distant,spot_vertices,spot_times)
    radii = ones(numel(spot_times),1);
    spots_distant.Set(spot_vertices,spot_times,radii);
end