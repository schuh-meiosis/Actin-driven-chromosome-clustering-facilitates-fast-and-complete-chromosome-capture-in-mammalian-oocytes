
# Pairwise Distance

---

##Overview:
	Determines the two vertices of an Imaris surface that are the furthest apart and marks them with spots.
	
---
	
##Installation
	No installation required, just copy the folder to an accessible location and navigate to it in Matlab. All required functions are eithere there or in the subfolder "private"
	
---

##Requirements
### Imaris 8.4.1 or older
		Xtensions module
	
### Matlab
		Tested on Matlab R2018b
		Matlab Statistics and Machine Learning Toolbox

---

## Description
	Before Imaris 9.0, surface objects/segmentations were defined using vertices and surface normals. This script uses the Matlab function *pdist* to compute all pairwise 
	distances between these vertices. The pair with the largest distance is highlighted by creating spots in their locations. The surface normals are being ignored.
	Only the largest segment? Per frame?
	
##Usage
	1. create surfaces in Imaris
	run in Matlab:
	2. imaris = GetImaris_pathed(*path to ImarisLib.jar in imaris folder*)
	3. XTmax_distance_on_surface(imaris)
	
		
---

## Troubleshooting:
	When creating the object "imaris" in step 2, the ImarisLib.jar file has to be the correct one. If for example Imaris 9.0 and 8.4 are installed, the file from the Imaris 8.4 installation folder must be used. Otherwise, different types of errors can occur.
	
	Randomly, the communication between Imaris and Matlab can fail with random errors. In these cases Imaris and Matlab must be completely restarted.
	
	In case of too large surfaces, the number of vertices might become too large and this script will fail consistently. Increasing the value for surface detail in Imaris might help reduce the number of vertices.
	
	Reading and writing statistics in Imaris requires a lot of java heap memory. You can increase this in MATLABs "Preferences" under "MATLAB/General/Java Heap Memory"
---
	
##Contact
	eike-urs.moennich@mpinat.mpg.de
---

##References:
