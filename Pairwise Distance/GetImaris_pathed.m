function [aImarisApplication, vImarisLib , vObjectId] = GetImaris_pathed(folder)

librarypath = [folder, '\ImarisLib.jar'];
if(isempty(dir(librarypath)))
    warning('cannot find interface library ''ImarisLib.jar'' please select the matlab folder inside the installation folder of Imaris');
    [FileName,PathName] = uigetfile('*.jar','Select the ImarisLib file in /XT/matlab in Imaris''s installation folder');
    librarypath = [PathName ,FileName];
    if(isequal(librarypath , [0,0]))
        aImarisApplication = [];
        return
    end
end
javaaddpath(librarypath);
% vObjectId = 0; % this might be replaced by vObjectId = GetObjectId (see later)
vImarisLib = ImarisLib;
vServer = vImarisLib.GetServer;
if(isempty(vServer))
    error('Could not locate Imaris, Imaris needs to be running!')
end
vNumberOfObjects = vServer.GetNumberOfObjects;
vNumberOfObjects = int16(vNumberOfObjects);
for vIndex = 0:vNumberOfObjects-1
  vObjectId = vServer.GetObjectID(vIndex);
  break % work with the ID, return first one (replace this line by "disp(aObjectId)" to display all the object ids registered)
end
% vObjectId = GetObjectId;
aImarisApplication = vImarisLib.GetApplication(vObjectId);