    %use sum over the whole z-stack to trigger 2nd experimental settings
%only if the 2 last positions are triggered after another, the
%jobcontrol.txt is written twice very fast after another, so that the macro
%itself never acquires the secondary group
%just wait at the sacrificial position, get time passed between last 2
%images at this position, subtract this from the intended interval
%TODO: catch if there are not enough time points for the sac position
%recorded

%15.1.2019: added saving of variables to matlab
%           data file
%16.1.2019: changed setting values in size_largest_CC to maybe fix major
%hardcoded!
%7.2.2019: removing sacrificial position for testing
%20.3.2019: now skipping wait for last position, this allows starting of
%the macro after the first round of positions was acquired
%22.3.2019: now dealing better with cancels on input windows
function size_largest_CC = detect_NEBD_v11()

    bfpath = 'F:\Eike\MatlabToolboxes\bfmatlab';
    bfpath_2 = 'D:\Data\Alex\bfmatlab';

    if(~isdeployed)
        addpath(bfpath);
    end
    javaaddpath([bfpath '\bioformats_package.jar']);
    javaaddpath([bfpath_2 '\bioformats_package.jar']);

    options = get_user_input;
    if(isempty(options))
        return
    end
    %check inputs
    options = process_inputs(options);

    %initializations
    tic;
    situation.time_cur = toc;
    status = situation.time_cur < options.time_max;

    situation = initialize_situation(options);

    filenames_scenes = strings(situation.number_scenes,1 );
    size_largest_CC = -1*ones(10,situation.number_scenes);

    progress_bar = waitbar(0,['frame: ' num2str(situation.frame_cur),' scene: ', num2str(situation.scene_cur)],'CreateCancelBtn',...
                'setappdata(gcbf,''canceling'',1)');
    set(progress_bar,'Position',[200 500 300 100])
    setappdata(progress_bar,'canceling',0)        
    draw_waitbar(progress_bar,situation,options)

    % fast_forward_to_current_position() TODO

    while(status)
        %Do high resolution part (Triggered)
        if getappdata(progress_bar,'canceling')
            break;
        end
            
        if(~isempty(situation.scene_ids_triggered))
            
            
            status = wait_for_first_file(options,situation,progress_bar,status); %wait for a file with 'preview' to be sure that the first scene is acquired with current settings
            situation.time_last = toc;
            set_interval_zero(options,situation);
            situation.group_imaged = "secondary";
            status = wait_for_last_file_trig(options,situation,progress_bar,status);                            
            
            write_jobcontrol_to_primary(options,situation);
        end
        
        status = min(status , situation.time_cur <= options.time_max && ~isempty(situation.scene_ids_active));%if stop was issued already before, dont ignore
        
        %Do low resolution part (Triggering)
        %check cancelbutton on waitbar
        situation.group_imaged = "primary";
        while(status)
        
            if getappdata(progress_bar,'canceling')
                break;
            end

            situation.time_cur =toc;
            status = situation.time_cur <= options.time_max && ~isempty(situation.scene_ids_active);

            situation = advance_next_file(situation);

            draw_waitbar(progress_bar,situation,options)
    %         if(situation.first_scene)%set interval to 0 after acquisition of first scene to allow out of order acquisition without delay
    %             
    %             status = wait_for_writing_file(options,situation,progress_bar,status); %wait for a file with 'preview' to be sure that the first scene is acquired with current settings
    %             
    %             set_interval_zero(options,situation);
    %         end
            if(situation.last_scene)%start trigger if anything was triggered yet and on last scene to synchronize position and exp index

                situation = switch_position_to_secondary(situation,options);

                status = wait_for_writing_file(options,situation,progress_bar,filenames_scenes,status); %wait for a file with 'preview' to be sure that the last scene is acquired with current settings
                situation.time_cur = toc;
%                 write_jobcontrol_to_secondary(options,situation);
                set_interval(options,situation);
                [full_filename,filenames_scenes,status] = get_image_file(options,filenames_scenes,situation,progress_bar,status);

                [volume_current,size_largest_CC,status] = get_nucleus_intensity_sphere(full_filename,options,situation,size_largest_CC,progress_bar,status);
                
                if(status>-1)
                    situation = determine_situation(situation,size_largest_CC,options);
                end
                plot(size_largest_CC);
                drawnow;
                break; %break out of primary loop, get to triggered group

            end

            [full_filename,filenames_scenes,status] = get_image_file(options,filenames_scenes,situation,progress_bar,status);
            situation.time_cur = toc;

            [volume_current,size_largest_CC,status] = get_nucleus_intensity_sphere(full_filename,options,situation,size_largest_CC,progress_bar,status);

            %check for trigger, missed etc
            if(status>-1)
                situation = determine_situation(situation,size_largest_CC,options);
            end
            plot(size_largest_CC);
            drawnow;
        end
    end

%     status = wait_for_sacrifice_file(options,situation,progress_bar,status);
    if(isempty(situation.scene_ids_active))
        set_interval_default(options,situation);
    end
    
    save([get_path_log(options) '/matlabdata.mat'])
    
    delete(progress_bar)
    disp('finished')
end

% function execute_trigger_start(options,situation)
%     path_JobControl = get_path_jobcontrol(options);
% %     situation.time_cur = toc;
%     
%     if(situation.group_imaged == "primary")
%         
%         
%         if(~isempty(situation.scene_ids_triggered)) %dont trigger on an empty group (e.g. before any position is triggered)
%             experiment_number =2;
%             ref_channel=options.ref_channel_2;
%             positions = [situation.scenes_names( situation.scene_ids_triggered)-1  ]; %counting starts at 0
% %             positions = [situation.scenes_names( situation.scene_ids_triggered)-1 ]; %counting starts at 0, add sacrificial position
%             %af macro gets time since last acquisition and waits until this
%             %time reaches the interval, but the sacrificial position is
%             %acquires in both groups, therefore the 2nd to last acquisition
%             %actually matters, therefore get the difference between the two
%             %and subtract it from the interval
% %             predicted_filename_full_part = [options.filename_base sprintf(options.ending_template,situation.scenes_names( end))];
% %             files_current_scene =dir(predicted_filename_full_part);
% %             times = sort(cell2mat({files_current_scene.datenum})); %the unit is days
% %             time_between = (times(end) - times(end-1))*3600*24;
%             time_between = (situation.times_started_sacrificial(end) - situation.times_started_sacrificial(end-1));
% %             time_spent= mod(situation.time_cur,options.interval_org);
% %             interval = options.interval_org - time_spent; %seconds, asap
% %             if(numel(situation.scene_ids_active)==0)
% %                 interval=options.interval_org;
% % %                 interval=options.interval_org - time_between;
% %             else
%             if(~exist('timerVal'))
%                 timerVal = tic;
%                 situation.time_cur = options.interval_org;
%             else
%                 situation.time_cur = toc(timerVal);
%             end
%             if(~exist('time_index'))
%                 time_index = 0;
%             else
%                 time_index = time_index + 1;
%             end
%             time_since_first = situation.time_cur - time_index * options.interval_org;
%             interval = time_since_first - time_between;
% %             interval=options.interval_org - time_between;
% %                 interval=options.interval_org - situation.times_started_sacrificial(end);
% %             end
%             
% %             interval=0; %seconds, asap
% %             interval=98000;
%         else
% %             experiment_number =2;
% %             ref_channel=options.ref_channel_2;
% %             positions = [situation.scenes_names( situation.scene_ids_triggered)-1  situation.scenes_names( end)-1]; %counting starts at 0, add sacrificial position
% %             interval=options.interval_org;
%             return
%         end
%     else
%         if(~isempty(situation.scene_ids_active))%dont trigger on an empty primary group (e.g when all positions are triggered)
%             experiment_number =1;
%             ref_channel=options.ref_channel_1;
%             positions = [situation.scenes_names( situation.scene_ids_active)-1 ]; %counting starts at 0
% %             positions = [situation.scenes_names( situation.scene_ids_active)-1 ]; %counting starts at 0, add sacrificial position
%             interval=0; %seconds, asap
%         else
%             %if all have been triggered the interval can be controlled by
%             %the af macro again
% %             experiment_number =2;
% %             ref_channel=options.ref_channel_2;
% %             positions = [situation.scenes_names( situation.scene_ids_triggered)-1  situation.scenes_names( end)-1]; %counting starts at 0, add sacrificial position
% %             interval=options.interval_org;
%             return
%         end
%     end
%     
%     write_JobControl(path_JobControl,experiment_number,ref_channel,positions,interval)
% 
% end

function write_jobcontrol_to_primary(options,situation)
    path_JobControl = get_path_jobcontrol(options);

    if(~isempty(situation.scene_ids_active))%dont trigger on an empty primary group (e.g when all positions are triggered)
        experiment_number =1;
        ref_channel=options.ref_channel_1;
        positions = [situation.scenes_names( situation.scene_ids_triggered)-1  situation.scenes_names( situation.scene_ids_active)-1]; %counting starts at 0
%             positions = [situation.scenes_names( situation.scene_ids_active)-1 ]; %counting starts at 0, add sacrificial position
        interval=0; %seconds, asap
    else
        %if all have been triggered the interval can be controlled by
        %the af macro again
%             experiment_number =2;
%             ref_channel=options.ref_channel_2;
%             positions = [situation.scenes_names( situation.scene_ids_triggered)-1  situation.scenes_names( end)-1]; %counting starts at 0, add sacrificial position
%             interval=options.interval_org;
        return
    end

    
    write_JobControl(path_JobControl,experiment_number,ref_channel,positions,interval)

end

function write_jobcontrol_to_secondary(options,situation)
    path_JobControl = get_path_jobcontrol(options);
  
    if(~isempty(situation.scene_ids_triggered)) %dont trigger on an empty group (e.g. before any position is triggered)
        experiment_number =2;
        ref_channel=options.ref_channel_2;
        positions = [situation.scenes_names( situation.scene_ids_triggered)-1  situation.scenes_names( situation.scene_ids_active)-1]; %counting starts at 0

%         time_between = (situation.times_started_sacrificial(end) - situation.times_started_sacrificial(end-1));

%         if(~exist('timerVal'))
%             timerVal = tic;
%             situation.time_cur = options.interval_org;
%         else
%             situation.time_cur = toc(timerVal);
%         end
%         if(~exist('time_index'))
%             time_index = 0;
%         else
%             time_index = time_index + 1;
%         end
%         time_since_first = situation.time_cur - time_index * options.interval_org;
%         interval = time_since_first - time_between;
        interval =0;
    else

        return
    end
    
    write_JobControl(path_JobControl,experiment_number,ref_channel,positions,interval)

end

function positions=get_positions(options)
    path_JobControl = get_path_jobcontrol(options);
    fid = fopen([path_JobControl 'JobControl.txt'],'r');
    if(fid ==-1)
        errordlg(['Could not find file JobControl.txt in: ' path_JobControl])
    end
    try
        JobControl = fgetl(fid);
        JobControl(1:17)=[];
        experiment_number = str2num(JobControl);
        JobControl = fgetl(fid);
        JobControl(1:24)=[];
        ref_channel_org = str2num(JobControl);
        JobControl = fgetl(fid);
        JobControl(1:25)=[];
        positions = str2num(JobControl);
        JobControl = fgetl(fid);
        JobControl(1:9)=[];
        interval =  str2num(JobControl);
        positions = positions+1;
    catch
        error(['file JobControl.txt in: ' path_JobControl ' seems to be corrupted or opened in another program, has to contain 6 lines with descriptions and values'])
    end
    fclose(fid);

end

function write_JobControl(path_JobControl,experiment_number,ref_channel,positions,interval)
% Experiment Index:1
% Reference Channel Index:2
% Active Positions Indices:0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24
% Interval:1.0
    try
        fid = fopen([path_JobControl 'JobControl.txt'],'wt');
        fprintf(fid, ['Experiment Index:' num2str(experiment_number) '\n']);
        fprintf(fid, ['Reference Channel Index:' num2str(ref_channel) '\n']);
        position_string =num2str(positions(1));
        for i=2:numel(positions)
            position_string = [position_string ',' num2str(positions(i))];
        end
        fprintf(fid, ['Active Positions Indices:' position_string '\n']);
        fprintf(fid, ['Interval:' '%6.1f' '\n'],interval );
        fprintf(fid, ['Show Exp Image:False \n']);
        fprintf(fid, ['Show AF Image:True']);
        fclose(fid);
        clear fid
    catch
        warning('could not write JobControl.txt')
        if(exist('fid'))
            fclose(fid);
        end
    end
end

function write_JobControl_stop(path_JobControl)
% Experiment Index:1
% Reference Channel Index:2
% Active Positions Indices:0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24
% Interval:1.0
    try
        fid = fopen([path_JobControl 'JobControl.txt'],'wt');
        fprintf(fid, ['Stop']);
        fclose(fid);
        clear fid
    catch
        warning('could not write JobControl.txt')
        if(exist('fid'))
            fclose(fid);
        end
    end
end

function options = get_user_input()
    %define user input box
    %prompt for filename and path of future image data
    % [FILENAME, PATHNAME] = uiputfile('5 positions 1 min-Split Scenes-0%d_frame_%d.czi', 'Please select a *.czi file which has already been created by the autofocus macro');
    [FILENAME, PATHNAME] = uigetfile('5 positions 1 min-Split Scenes-0%d_frame_%d.czi', 'Please select a *.czi file which has already been created by the autofocus macro');
    if(all(eq(FILENAME,double(0))) && all(eq(PATHNAME,double(0))))
        options = [];
        return;
    end
    [filename_base , ending_template] = get_filename_template(FILENAME, PATHNAME);
    %prompt for parameters of image data
    prompt = {'Enter dextran channel (starts at 1):',...
              'Enter tracking channel for triggered exp(starts at 1):',...
              'Enter duration of triggered settings [s]:',...
              'Enter maximum number of triggered positions [s]:',...
              'Enter maximum duration of the experiment [s]:',...
              'Enter threshold for difference from median: ',...
              'Enter threshold for difference in standard deviations: ',...
              'Enter filtersize for smoothing the image before automatically determining the nucleus [?m]',...
             };
    dlg_title = 'EXPECTS AF MACRO TO TRACK THE CHROMOSOME CHANNEL';
    num_lines = 1;
    
    defaultans = {'2','2','7200','5','90000','0.1','4','2'};
    answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    if(isempty(answer))
        options = [];
        return;
    end
    channel_dex = str2double(answer{1})-1;
    ref_channel_2 =str2double(answer{2})-1;
    duration_trigger = str2double(answer{3});
    positions_max = str2double(answer{4});
    time_max = str2double(answer{5});
    threshold_median =str2double(answer{6});
    threshold_std = str2double(answer{7});
    filtersize =  str2double(answer{8});

    if(~all(isfinite(str2double(answer))))
        return;
    end
    
    options.FILENAME = FILENAME;
    options.PATHNAME = PATHNAME;
    options.filename_base=filename_base;
    options.ending_template=ending_template;
    options.channel_dex = channel_dex;
    options.ref_channel_2 = ref_channel_2;
    options.duration_trigger = duration_trigger;
    options.positions_max = positions_max ;
    options.time_max=time_max;
    options.threshold_median=threshold_median;
    options.threshold_std=threshold_std;
    options.filtersize=filtersize;

end

function [filename_base , ending_template] = get_filename_template(FILENAME, PATHNAME)
   %the experimentname might contain p_1 ,p_2 .. etc, start searching the filename from the end
    %the number of scenes is unknown

    filename_base =[PATHNAME FILENAME(1:13) '*']; % the first 13 characters encode the date and time of the experiment ad stay consistent over different triggered experimental settings
%     filename_ending_template = 'p_%d_t_%d.czi';
    ending_template = 'p_%d_t_*.czi'; 
    
%     filename_template =[options.PATHNAME filename_base filename_ending_template_scene];
end

function path_jobcontrol = get_path_jobcontrol(options)
    position_last_folder = regexp(options.PATHNAME, '\\*');
    position_last_folder = position_last_folder(end-2);
    path_jobcontrol = options.PATHNAME(1:(position_last_folder));
end

function path_log = get_path_log(options)
    position_last_folder = regexp(options.PATHNAME, '\\*');
    position_last_folder = position_last_folder(end-1);
    path_log = options.PATHNAME(1:(position_last_folder));
end

function options = process_inputs(options)
    reader = bfGetReader([options.PATHNAME options.FILENAME]);
    omeMeta = reader.getMetadataStore();
    iSeries =1;
    reader.setSeries(iSeries - 1);
    stackSizeC = omeMeta.getPixelsSizeC(0).getValue;
    stackSizeZ = omeMeta.getPixelsSizeZ(0).getValue; % number of Z slices
    options.size_Z=stackSizeZ;
    if(options.size_Z<2)
        errordlg('Found just a single Z-Slice, was the file completed writing?')
    end
    if(options.channel_dex>=stackSizeC)
        warndlg(['channel not available switching to channel number: ' num2str(stackSizeC)])
        options.channel_dex =stackSizeC-1;
    end
%     if(options.channel_chr>=stackSizeC)
%         warndlg(['channel not available switching to channel number: ' num2str(stackSizeC)])
%         options.channel_chr =stackSizeC-1;
%     end
    if(options.channel_dex<0)
        warndlg(['channel not available switching to channel number: 1']);
        options.channel_dex =0;
    end 
%     if(options.channel_chr<0)
%         warndlg(['channel not available switching to channel number: 1']);
%         options.channel_chr =0;
%     end 
    if(options.time_max<0)
        errordlg(['Maximum experimental duration must be a positive number']);
    end 
    
    path_JobControl = get_path_jobcontrol(options);
    
    fid = fopen([path_JobControl 'JobControl.txt'],'r');
    if(fid ==-1)
        errordlg(['Could not find file JobControl.txt in: ' path_JobControl])
    end
    try
        JobControl = fgetl(fid);
        JobControl(1:17)=[];
        experiment_number_org = str2num(JobControl);
        JobControl = fgetl(fid);
        JobControl(1:24)=[];
        ref_channel_org = str2num(JobControl);
        JobControl = fgetl(fid);
        JobControl(1:25)=[];
        positions_org = str2num(JobControl);
        JobControl = fgetl(fid);
        JobControl(1:9)=[];
        interval_org =  str2num(JobControl);
        if(isempty(experiment_number_org))
            warndlg(['file JobControl.txt in: ' path_JobControl ' seems to be corrupted, could not read experiment index, switching to 1'])
            experiment_number_org=1;
        end
        if(isempty(experiment_number_org)|| isempty(ref_channel_org) || isempty(positions_org) || isempty(interval_org))
            error(['file JobControl.txt in: ' path_JobControl ' seems to be corrupted, has to contain 6 lines with descriptions and values'])
        end
    catch
        error(['file JobControl.txt in: ' path_JobControl ' seems to be corrupted, has to contain 6 lines with descriptions and values'])
    end
    fclose(fid);
    options.ref_channel_1 = ref_channel_org;
    options.interval_org = interval_org;
end

function [full_filename,filenames_scenes,status] = get_image_file(options,filenames_scenes,situation,progress_bar,status)
    full_filename="";
    while(full_filename=="")%repeat if file still could not be read
        try
            predicted_filename_full_part = [options.filename_base sprintf(options.ending_template,situation.scene_cur)];
        
            files_current_scene = dir(predicted_filename_full_part);
            number_frames_current_scene = length(files_current_scene);
            preview_string = 'preview.czi';

            if(~isempty(files_current_scene))
%                 if(strlength(filenames_scenes(situation.scene_cur_id))==0 || (situation.frame_cur==1))
                if(strlength(filenames_scenes(situation.scene_cur_id))==0)
                    if(~any(contains({files_current_scene.name},preview_string)))
                        files_current_scene_sorted = sortrows(struct2table(files_current_scene),3);%sort by creation date
                        filenames_scenes(situation.scene_cur_id) = string(files_current_scene_sorted(1,1).name);
                        full_filename = char(strcat(options.PATHNAME,filenames_scenes(situation.scene_cur_id)));
                    else
                        pause(1)
                        full_filename ="";
                    end
                else
            
                    if(~any(contains({files_current_scene.name},preview_string)))
                        files_current_scene_sorted = sortrows(struct2table(files_current_scene),3);%sort by creation date
                        index_now = find(string(files_current_scene_sorted(:,1).name)== filenames_scenes(situation.scene_cur_id));
%                         if(number_frames_current_scene>=situation.frame_cur)
                        if(number_frames_current_scene > index_now )
%                             filename_next = string(files_current_scene_sorted(situation.frame_cur,1).name);
                            filename_next = string(files_current_scene_sorted(index_now+1,1).name);%always take the next file
                            full_filename = char(strcat(options.PATHNAME,filename_next));
                            filenames_scenes(situation.scene_cur_id) = filename_next;
                        else
                             pause(1)
                            full_filename ="";
                        end
                    else
                        pause(1)
                        full_filename ="";
                    end

                end
            else
                pause(1)
                full_filename ="";
            end
            
            %check cancelbutton on waitbar
            if getappdata(progress_bar,'canceling')
                status = -1;
                return
            end
            
        catch
            pause(1)
            full_filename ="";
        end
    end
end

function situation = determine_situation(situation,size_largest_CC,options)

    if(~ismember(situation.scene_cur_id,situation.scene_ids_active))
        return;
    end

%     jump_size_allowed = options.threshold_median*median(size_largest_CC(1:situation.frame_cur-1,situation.scene_cur_id))+ options.threshold_std*std(size_largest_CC(1:situation.frame_cur-1,situation.scene_cur_id));
    jump_size_allowed = options.threshold_median*median(size_largest_CC(1:situation.frame_cur-1,situation.scene_cur_id))+ options.threshold_std*std(size_largest_CC(1:situation.frame_cur-1,situation.scene_cur_id));
%     difference = (median(size_largest_CC(1:situation.frame_cur-1,situation.scene_cur_id)) - size_largest_CC(situation.frame_cur,situation.scene_cur_id));
    difference = size_largest_CC(situation.frame_cur,situation.scene_cur_id) - median(size_largest_CC(1:situation.frame_cur-1,situation.scene_cur_id));
    if(situation.frame_cur>3)
%         situation.just_triggered = difference > jump_size_allowed | size_largest_CC(situation.frame_cur,situation.scene_cur_id)<options.minimum_size;
        situation.just_triggered = difference > jump_size_allowed ;
    else
%         situation.just_triggered = size_largest_CC(situation.frame_cur,situation.scene_cur_id)<options.minimum_size;
    end
    
    situation.difference(situation.frame_cur,situation.scene_cur_id) = difference;
    situation.jump_size_allowed(situation.frame_cur,situation.scene_cur_id) = jump_size_allowed;
    situation.scene(situation.frame_cur,situation.scene_cur_id) = situation.scene_cur;
    situation.file(situation.frame_cur,situation.scene_cur_id) = situation.file_cur;
%     situation.just_triggered = false;
%     situation.just_triggered = situation.just_triggered  ||(situation.frame_cur == 10 && situation.scene_cur_id ==1);
%     situation.just_triggered = situation.just_triggered  ||(situation.frame_cur == 4 && situation.scene_cur_id ==2);
%     situation.just_triggered = situation.just_triggered  ||(situation.frame_cur == 8 && situation.scene_cur_id ==3);
%     situation.just_triggered = situation.just_triggered  ||(situation.frame_cur == 11 && situation.scene_cur_id ==4);
%     situation.just_triggered = situation.just_triggered  ||(situation.frame_cur == 109 && situation.scene_cur_id ==5);
%     situation.just_triggered = situation.just_triggered  ||(situation.frame_cur == 56 && situation.scene_cur_id ==6);
%     situation.just_triggered = situation.just_triggered  ||(situation.frame_cur == 6 && situation.scene_cur_id ==7);

    if(size_largest_CC(situation.frame_cur,situation.scene_cur_id) <0)
        situation.just_triggered = false;
    end
    %in case the last position was triggered last and actually got imaged
    %in primary after it was remembered to be triggered
    if(ismember(situation.scene_cur_id,situation.scene_ids_to_be_triggered))
%         situation.time_cur = toc;
        situation.times_triggered(situation.scene_cur_id) = situation.time_cur;
    end
    if(situation.just_triggered)
%         situation = switch_position_to_secondary(situation,options);
        situation = reserve_position_for_triggering(situation);
    end

end

function situation = advance_next_file(situation)

    
    if(situation.last_scene)
        % deal with changing indices when the last scene in a group is
        % encountered
        
        situation.scene_cur_id = situation.scene_ids_active(1);
        situation.frame_cur = situation.frame_cur+1;
        
        situation.scene_cur = situation.scenes_names(situation.scene_cur_id);
        situation.file_cur = situation.file_cur+1;
        situation.last_scene = false;
        situation.first_scene = true;
        %if there is just one scene in the group, it always is the last
        %scene, "first scene" must be false for proper function
        if(situation.scene_cur_id == situation.scene_ids_active(end))
            situation.last_scene=true;
            situation.first_scene = false;
        end
        return;
    end
    
    %update frame/scene counter
    situation.file_cur = situation.file_cur+1;
    
    
    if(numel(situation.scene_ids_active)>1)
        situation.scene_cur_id = min(situation.scene_ids_active(situation.scene_ids_active>situation.scene_cur_id));
    else
        situation.scene_cur_id = situation.scene_ids_active(1);
    end

    situation.first_scene = false;
    situation.last_scene = false;
    situation.secondto_last_scene = false;
    
    if(situation.scene_cur_id == situation.scene_ids_active(1))
        situation.first_scene=true;
    end
    if(situation.scene_cur_id == situation.scene_ids_active(end))
        situation.last_scene=true;
    end
    if(numel(situation.scene_ids_active)>1)
        if(situation.scene_cur_id == situation.scene_ids_active(end-1))
            situation.secondto_last_scene=true;
        end
    end

    situation.scene_cur = situation.scenes_names(situation.scene_cur_id);
    
end

function situation = initialize_situation(options)
    situation.just_triggered=false;
    situation.scene_cur_id=1;
    situation.scenes_names=get_positions(options);
    situation.number_scenes = numel(situation.scenes_names );
    situation.scene_cur=situation.scenes_names(1);
    situation.scene_ids_active=1:situation.number_scenes; 
    situation.scene_ids_triggered=[];
    situation.scene_ids_to_be_triggered=[];%gather all scene ids that are detected to be triggered but trigger them when jobcontrol is written
    situation.frame_cur=0;
    situation.file_cur=0;
    situation.write_now=false;
    situation.last_scene=true;%start with true to advance to first scene in the beginning
    situation.secondto_last_scene=false;
    situation.first_scene=true;
    situation.group_imaged = "secondary";% primary(checking) or secondary(triggered for NEBD); start with secondary to switch  to the first position at the start
    situation.times_triggered=-1*ones(situation.number_scenes,1);
    situation.time_cur=0;
    situation.times_started_sacrificial = [];
    situation.time_last =0;
end

% function [output,status] = get_nucleus_volume(filename,options,situation,progress_bar,status)
% 
%     output=-1;
%     if(situation.group_imaged == "secondary")
%         return;
%     end
%     
%     file_read_succesfully=false;
%     while(~file_read_succesfully)
%         try% if file still cannot be read
%             if getappdata(progress_bar,'canceling')
%                 status = -1;
%                 return
%             end
%             copyfile(filename,'F:\Eike\dumpfile.czi');
%             reader = bfGetReader(filename);
%             
%             omeMeta = reader.getMetadataStore();
%             iSeries =1;
%             reader.setSeries(iSeries - 1);
%             stackSizeX = omeMeta.getPixelsSizeX(0).getValue(); % image width, pixels
%             stackSizeY = omeMeta.getPixelsSizeY(0).getValue(); % image height, pixels
%             stackSizeZ = omeMeta.getPixelsSizeZ(0).getValue(); % number of Z slices
%             stacksize = stackSizeZ;
%             if(stacksize ~= options.size_Z)
%                 pause(0.1);
%                 continue
%             end
%             pixelsize_x = double(omeMeta.getPixelsPhysicalSizeX(0).value);
%             pixelsize_y = double(omeMeta.getPixelsPhysicalSizeY(0).value);
%             pixelsize_z = double(omeMeta.getPixelsPhysicalSizeZ(0).value);
%             pixelvolume =  pixelsize_x * pixelsize_y * pixelsize_z;
%             times_max = str2double(char(omeMeta.getPixelsSizeT(0)));
%             iT = 1;
%             iC =options.channel_dex;
%             stack = zeros(stackSizeX,stackSizeY,stacksize);
%             filtersize_pxl = round(options.filtersize/pixelsize_x);
% 
%             for iZ=1:stacksize
%                 iPlane = reader.getIndex(iZ - 1, iC, iT - 1) + 1;
%                 plane_current = bfGetPlane(reader, iPlane);
%                 if(sum(abs(plane_current(:)))==0)
%                     error(['plane' num2str(iZ) ' at ' num2str(iT) ' is completely zero'])
%                 end
%                 stack(:,:,iZ) = plane_current;
%             end
% 
%             reader.close
%             if(filtersize_pxl>0 && filtersize_pxl<stackSizeX)
%                 smoothing_matrix = ones(filtersize_pxl)/filtersize_pxl^2;
%             else
%                 warndlg('filter too large or too small')
%             end
%             stack_smooth = imfilter(stack,smoothing_matrix);
%             thresh = multithresh(stack_smooth);
%             CC = bwconncomp(stack_smooth<thresh,6);
%             numPixels = cellfun(@numel,CC.PixelIdxList);
%             output = max(numPixels)*pixelvolume;
%             file_read_succesfully = true;
%         catch
%             pause(0.1)
%         end
%     end
% 
% end


function [output,size_largest_CC,status] = get_nucleus_intensity_sphere(filename,options,situation,size_largest_CC,progress_bar,status)

%     if(~isdeployed)
%         addpath(bfpath);
%     end
%     javaaddpath([bfpath '\bioformats_package.jar']);

    nucleusdiameter =10;
    
    output=-1;
    if(situation.group_imaged == "secondary")
        return;
    end

    file_read_succesfully=false;
    while(~file_read_succesfully)
        try% if file still cannot be read
            if getappdata(progress_bar,'canceling')
                status = -1;
                return
            end
            % reader = bfGetReader();
%             copyfile(filename,'F:\Eike\dumpfile.czi');
            copyfile(filename,[options.PATHNAME 'dumpfile.czi']);
            
            reader = bfGetReader(filename);
            omeMeta = reader.getMetadataStore();
            iSeries =1;
            reader.setSeries(iSeries - 1);
            stackSizeX = omeMeta.getPixelsSizeX(0).getValue(); % image width, pixels
            stackSizeY = omeMeta.getPixelsSizeY(0).getValue(); % image height, pixels
            stackSizeZ = omeMeta.getPixelsSizeZ(0).getValue(); % number of Z slices
            stacksize = stackSizeZ;
            if(stacksize ~= options.size_Z)
                pause(0.1);
                continue
            end
            pixelsize_x = double(omeMeta.getPixelsPhysicalSizeX(0).value);
            pixelsize_y = double(omeMeta.getPixelsPhysicalSizeY(0).value);
            pixelsize_z = double(omeMeta.getPixelsPhysicalSizeZ(0).value);
            % pixelvolume =  pixelsize_x * pixelsize_y * pixelsize_z;
            % times_max = str2double(char(omeMeta.getPixelsSizeT(0)));
            iT = 1;
            iC =options.channel_dex;
            iC_chr = options.ref_channel_1;
            stack_dex = zeros(stackSizeX,stackSizeY,stacksize);
            stack_chr = zeros(stackSizeX,stackSizeY,stacksize);


            for iZ=1:stacksize
                iPlane_dex = reader.getIndex(iZ - 1, iC, iT - 1) + 1;
                iPlane_chr = reader.getIndex(iZ - 1, iC_chr, iT - 1) + 1;
                plane_current_dex = bfGetPlane(reader, iPlane_dex);
                plane_current_chr = bfGetPlane(reader, iPlane_chr);
                if(sum(abs(plane_current_dex(:)))==0 || sum(abs(plane_current_chr(:)))==0)
                    error(['plane' num2str(iZ) ' at ' num2str(iT) ' is completely zero'])
                end
                stack_dex(:,:,iZ) = plane_current_dex;
                stack_chr(:,:,iZ) = plane_current_chr;
            end

            reader.close

            x = 0:pixelsize_x:pixelsize_x*(stackSizeX-1);
            y = 0:pixelsize_y:pixelsize_y*(stackSizeY-1);
            z = 0:pixelsize_z:pixelsize_z*(stackSizeZ-1);
            [X,Y,Z] = meshgrid(x,y,z);
            xx_chr = squeeze(sum(sum(stack_chr,1),3));
            xx_chr = xx_chr - min(xx_chr);
            yy_chr = squeeze(sum(sum(stack_chr,2),3));
            yy_chr = yy_chr - min(yy_chr);
            zz_chr = squeeze(sum(sum(stack_chr,2),1));
            zz_chr = zz_chr - min(zz_chr);
            com_x = sum(xx_chr .* (1:numel(xx_chr)))./sum(xx_chr) * pixelsize_x;
            com_y = sum(yy_chr .* (1:numel(yy_chr))')./sum(yy_chr) * pixelsize_y;
            com_z = sum(zz_chr .* (1:numel(zz_chr))')./sum(zz_chr) * pixelsize_z;
            com_distance = sqrt((X - com_x).^2 + (Y - com_y).^2 +(Z - com_z).^2);
            sphere = com_distance < (nucleusdiameter/2);

            output = mean(stack_dex(sphere));
            size_largest_CC(situation.frame_cur,situation.scene_cur_id)=output;
            file_read_succesfully = true;
        catch
            pause(0.1)
        end
    end
end

function status = wait_for_writing_file(options,situation,progress_bar,filenames_scenes,status)

    situation.write_now=false;
    while(~situation.write_now)
        
        %check cancelbutton on waitbar
        if getappdata(progress_bar,'canceling')
            status = -1;
            return
        end

%         if(situation.group_imaged == "primary")
%             if(~isempty(situation.scene_ids_active))
%                 if(situation.scene_ids_active(end) ~= situation.scene_cur_id)
%                     pause(0.1);
%                     return
%                 end
%             end
%         end
%         if(situation.group_imaged == "secondary")
%             if(~isempty(situation.scene_ids_triggered))
%                 if(situation.scene_ids_triggered(end) ~= situation.scene_cur_id)
%                     pause(0.1);
%                     return
%                 end
%             end
%         end
        
        
        
        predicted_filename_full_part = [options.filename_base sprintf(options.ending_template,situation.scene_cur)];
        files_current_scene =dir(predicted_filename_full_part);
        number_frames_current_scene = length(files_current_scene);
        files_current_scene_sorted = sortrows(struct2table(files_current_scene),3);%sort by creation date
        index_now = find(string(files_current_scene_sorted(:,1).name)== filenames_scenes(situation.scene_cur_id));
        if(isempty(index_now))
            index_now = 0;
        end
        preview_string = 'preview.czi';

        if(any(contains({files_current_scene.name},preview_string)))
            situation.write_now=true;    
        else
            if(number_frames_current_scene > index_now )
                situation.write_now=true;    
                return
            end
%             pause(0.1);
        end
        
        
    end
end

function status = wait_for_last_file(options,situation,progress_bar,status)

    situation.write_now=false;
    while(~situation.write_now)
        
        %check cancelbutton on waitbar
        if getappdata(progress_bar,'canceling')
            status = -1;
            return
        end

        predicted_filename_full_part = [options.filename_base sprintf(options.ending_template,situation.scenes_names(situation.scene_ids_active(end)))];
        files_current_scene =dir(predicted_filename_full_part);
%         files_all =dir;
        preview_string = 'preview.czi';

        if(any(contains({files_current_scene.name},preview_string)))
            situation.write_now=true;    
        else
            pause(0.1);
        end
        
        
    end
end

function status = wait_for_last_file_trig(options,situation,progress_bar,status)

    situation.write_now=false;
    while(~situation.write_now)
        
        %check cancelbutton on waitbar
        if getappdata(progress_bar,'canceling')
            status = -1;
            return
        end

        predicted_filename_full_part = [options.filename_base sprintf(options.ending_template,situation.scenes_names(situation.scene_ids_triggered(end)))];
        files_current_scene =dir(predicted_filename_full_part);
%         files_all =dir;
        preview_string = 'preview.czi';

        if(any(contains({files_current_scene.name},preview_string)))
            situation.write_now=true;    
        else
            pause(0.1);
        end
        
        
    end
end

function situation = reserve_position_for_triggering(situation)
%     situation.time_cur = toc;
    situation.times_triggered(situation.scene_cur_id) = situation.time_cur;
    situation.scene_ids_to_be_triggered = [situation.scene_ids_to_be_triggered situation.scene_cur_id];
end


function situation = switch_position_to_secondary(situation,options)

    %move scene id from primary group to the secondary group
    for i=1:numel(situation.scene_ids_to_be_triggered)
        
        id_replace=-1;
        %check if a spot got free
%         for j=1:numel(situation.scene_ids_triggered)
%             time_imaged = situation.time_cur - situation.times_triggered(situation.scene_ids_triggered(j));
%             if(time_imaged > options.duration_trigger)
%                 id_replace=j;
%                 break
%             end   
%         end
        %check if free spots are available
        if(numel(situation.scene_ids_triggered) < options.positions_max)
            id_replace=numel(situation.scene_ids_triggered)+1;
        else
            %find longest running position
            position_oldest = 1;
            for j=1:numel(situation.scene_ids_triggered)
                if(situation.times_triggered(situation.scene_ids_triggered(j)) < situation.times_triggered(situation.scene_ids_triggered(position_oldest)))
                    position_oldest = j;
                end
            end
            %check if the longest running position has run long enough for replacement
            time_imaged = situation.time_cur - situation.times_triggered(situation.scene_ids_triggered(position_oldest));
            if(time_imaged > options.duration_trigger)
                id_replace=position_oldest;
            end   
        end
        %remove triggered scene from primary group either way
        situation.scene_ids_active(situation.scene_ids_active==situation.scene_ids_to_be_triggered(i)) = [];
        
        %throw away position if no spot is available
        if(id_replace~=-1)
            %add scene
            situation.scene_ids_triggered(id_replace)= situation.scene_ids_to_be_triggered(i);
        end
    end
    situation.scene_ids_to_be_triggered =[];
end

function draw_waitbar(progress_bar,situation,options)
    try
        if(situation.just_triggered)
                waitbar_string = {['frame: ' num2str(situation.frame_cur),' scene: ', num2str(situation.scenes_names(situation.scene_cur_id)) ],...
                                [' group: ' char(situation.group_imaged) ' TRIGGERED! '],...
                                ['primary group: ' num2str(situation.scenes_names(situation.scene_ids_active)) ],...
                                ['secondary group: ' num2str(situation.scenes_names(situation.scene_ids_triggered))]};
        else
                waitbar_string = {['frame: ' num2str(situation.frame_cur),' scene: ', num2str(situation.scenes_names(situation.scene_cur_id)) ],...
                                [' group: ' char(situation.group_imaged)],...
                                ['primary group: ' num2str(situation.scenes_names(situation.scene_ids_active)) ],...
                                ['secondary group: ' num2str(situation.scenes_names(situation.scene_ids_triggered))]};
                
        end
        waitbar(situation.time_cur/options.time_max,progress_bar,waitbar_string);
    end
end

function set_interval_zero(options,situation)
    path_JobControl = get_path_jobcontrol(options);
    situation.time_cur = toc;

        if(~isempty(situation.scene_ids_active))%dont trigger on an empty primary group (e.g when all positions are triggered)
            
            experiment_number =2;
            ref_channel=options.ref_channel_2;
            positions = [situation.scenes_names( situation.scene_ids_triggered)-1 ]; %counting starts at 0
%             positions = [situation.scenes_names( situation.scene_ids_triggered)-1  ]; %counting starts at 0, add sacrificial position
            interval=0; %seconds, asap
        else

            return
        end
%     end

    write_JobControl(path_JobControl,experiment_number,ref_channel,positions,interval) 
end


function set_interval_default(options,situation)
    path_JobControl = get_path_jobcontrol(options);
    situation.time_cur = toc;
%     if(situation.group_imaged == "primary")
%         if(~isempty(situation.scene_ids_triggered)) %dont trigger on an empty group (e.g. before any position is triggered)
    experiment_number =2;
    ref_channel=options.ref_channel_2;
    positions = [situation.scenes_names( situation.scene_ids_triggered)-1  ]; %counting starts at 0
    interval=options.interval_org; %seconds
%             time_spent= mod(situation.time_cur,options.interval_org);
%             interval = options.interval_org - time_spent; %seconds, asap
%             if(numel(situation.scene_ids_active)==0)
%                 interval=options.interval_org;
%             end
%         else
%             return
%         end
%     else
%         if(~isempty(situation.scene_ids_active))%dont trigger on an empty primary group (e.g when all positions are triggered)
%             experiment_number =2;
%             ref_channel=options.ref_channel_2;
%             positions = [situation.scenes_names( situation.scene_ids_triggered)-1  situation.scenes_names( end)-1]; %counting starts at 0, add sacrificial position
% %             positions = [situation.scenes_names( situation.scene_ids_triggered)-1  ]; %counting starts at 0, add sacrificial position
%             interval=97000; %seconds
%         else
%             return
%         end
%     end

    write_JobControl(path_JobControl,experiment_number,ref_channel,positions,interval) 
end

function set_interval(options,situation)
    path_JobControl = get_path_jobcontrol(options);
%     if(situation.group_imaged == "primary")
%         if(~isempty(situation.scene_ids_triggered)) %dont trigger on an empty group (e.g. before any position is triggered)
    
    positions = [situation.scenes_names( situation.scene_ids_triggered)-1  situation.scenes_names( situation.scene_ids_active)-1]; %counting starts at 0
    
    %keep interval for first position in high res consistent
    %this can sometimes be a different position
    %keep track of all timings of all active non-triggered positions
    %set interval to necessary value relative to last time this position
    %was imaged and how much time is actually needed to be waited
%     situation.time_last %time when the first triggered position was last acquired
%     situation.time_cur = toc;
    
    
    %time difference should be default interval minus the time between the
    %last time the first triggered position was acquired to the last time
    %the new first triggered position was acquired
    if(~isempty(situation.scene_ids_triggered))
        experiment_number =2;
        ref_channel=options.ref_channel_2;
        time_difference = situation.times_triggered(situation.scene_ids_triggered(1)) - situation.time_last;
        if(time_difference < 0)
            time_difference =0;
        end
        interval=min(options.interval_org, options.interval_org-time_difference); 
    else
        interval=options.interval_org;
        experiment_number =1;
        ref_channel=options.ref_channel_1;
    end
    %also have to deal with last position triggering and the last
    %acquisition being not the time when recorded in situation.times_triggered
    write_JobControl(path_JobControl,experiment_number,ref_channel,positions,interval) 
end

function status = wait_for_first_file(options,situation,progress_bar,status)
   situation.write_now=false;
    while(~situation.write_now)
        
        %check cancelbutton on waitbar
        if getappdata(progress_bar,'canceling')
            status = -1;
            return
        end

        %find a file not belonging to the current position with "preview" in
        %its name
        predicted_filename_full_part = [options.filename_base sprintf(options.ending_template,situation.scene_cur)];
%         files_current_scene =dir(predicted_filename_full_part);
        files_all =dir(options.PATHNAME);
        files_current_scene =dir(predicted_filename_full_part);
        files_not_primary = files_all(~contains({files_all.name},{files_current_scene.name}));
        preview_string = 'preview.czi';

        if(any(contains({files_not_primary.name},preview_string)))
            situation.write_now=true;    
        else
            pause(0.1);
        end
        
        
    end 
end
