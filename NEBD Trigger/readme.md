
# NEBD Trigger

---

##Overview:
	This script is designed to run concurrently with an xyztc imaging session on a Zen LSM confocal microscope.
	Determines NEBD based on the influx of dextrans into the nucleus to trigger a second set of imaging settings for the automatic autofocus solution provided bei Zeiss.
	
---
	
##Installation
	No installation required, just copy the folder to an accessible location and navigate to it in Matlab. All required functions are eithere there or in the subfolder "private"
	
	Alternative: compile the script with Matlab once and run the resulting executable file (free Matlab Compiler Runtime required). This method does not require a Matlab license on the microscope computer.
	
---

##Requirements
### Zen 2
### Autofocus Macro Solution by Zeiss v2.1.0b
### Bioformats package
### Matlab or Matlab Compiler Runtime
		Tested on Matlab R2018b and Matlab Compiler Runtime

---

## Description
	For this script to work, there need to be at least two channels available in the object being imaged. The first is Dextran which flows into the nucleus upon NEBD.
	The second is chromatin. This channel is used to find the middle of the nucleus via center of mass.
	A sphere of specified radius is placed at the center of mass of the chromosome channel. Then the intensity of the dextran channel is measured within. 
	If the intensity increases relative to the last few frames, the autofocus script is instructed to switch the imaging conditions for this cell. 
	Parameters for the required change can be set.
	
	Triggered positions are imaged concurrently with non-triggered position. This allows to image triggered positions with high intensity while keeping phototoxicity overall low.
	Furthermore, more positions can be imaged in total. Imaging a position before triggering takes less time per frame than after triggering. If all positions were constantly imaged in high intensity, long exposure settings, there would be less time available.
	The interval between two frames does stay constant between both settings.
	The script is set up in a way that the interval during triggered settings stays as constant as possible. The exact interval between frames of non-triggered positions can vary due to lower priority.
	The number of positions being imaged is determined by reading the JobControl.txt created by the autofocus macro.
	A single position can be triggered just once. When a position finished being imaged in triggered settings, it is not being imaged in either setting anymore. A triggered position is finished imaging when the minimum imaging time has been reached and a new position was triggered to take its place.
	When more than the maximum number of positions was triggered and none of them has exceeded the minimum imaging time yet, the new ones will be ignored onwards.
	The positions will still be tracked by the autofocus macro when triggered. A appropriate channel must be available.
		
##Usage

The settings for thresholds and smoothing depend on the exact imaging conditions. To find good parameters, it is advised to run a preparatory experiment under the same conditions and adjust the parameters of this script to detect NEBD and the optimal time.

	1. The location of the bioformats package is written at the start of the code. This must be set correctly for the script to run successfully.
	2. set up two experimental settings in Zen
		One setting must be the primary one, where the chromatin and the dextran can be imaged
		The secondary setting is the one being triggered. Here, at least the chromatin channel must be available for continued tracking.
	3. start an experiment with the autofocus macro, tracking the primary channel
	4. start this script
	5. choose one of the image files saved in the target folder of the autofocus tracking macro
	
	Options:
	The first GUI asks for the location of any .czi file that was created by the experiment already. This is the folder where all czi files are going to appear during the running experiment.
	
	Dextran channel
		this channel will be measured within a 10um sphere to detect NEBD
	tracking channel
		this channel will be used by the tracking macro to keep focus on the cell after triggering
	duration of triggered settings
		this is the minimum time a triggered position will be imaged in seconds, such a position will be imaged longer if no other position is triggered
	maximum number of triggered positions
		if more than this number of positions has been triggered and has not reached their minimum imaging duration yet, new positions will be discarded.
	maximum duration of the experiment
		After this amount of time in seconds the script will terminate automatically
	threshold for difference from median
		a position is triggered if the current intensity is higher than the median of all previous frames of this position plus the allowable offset.
		The allowable offset is determined by the this threshold times the median of all previous frames plus the standard deviation of all previous frames times the next parameter
	threshold for difference in standard deviations
		a position is triggered if the current intensity is higher than the median of all previous frames of this position plus the allowable offset.
		The allowable offset is determined by the the previous threshold times the median of all previous frames plus the standard deviation of all previous frames times this parameter
	filtersize for smoothing the image before automatically determining the nucleus
		this parameter currently has no effect whatsoever
	
---

## Troubleshooting:
	It is very tricky to get triggering working this way. The versions of the autofocus tracking script we used were suffering from a bug.
	When switching to a single position, two images with different settings were combined into a single file, making analysis impossible.
	More structured approaches to triggering generally led to errors.
	
---
	
##Contact
	eike-urs.moennich@mpinat.mpg.de
---

##References:
	Customized Sample Tracking Solution provided by ZEISS Microscopy
