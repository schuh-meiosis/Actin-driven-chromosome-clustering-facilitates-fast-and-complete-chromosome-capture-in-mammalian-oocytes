% File: ReadImage6D.m
% Author: Sebastian Rhode
% Date: 18.06.2015
% Version: 1.1
% modified by Eike Urs M?nnich

% Read CZI image data into image6d array
% first Series ,last Series , Channel, Time, Z-Slice

function out = ReadImage6D3(filename,varargin)

% Get OME Meta-Information
MetaData = GetOMEData(filename);

Series_list_first = 1;
Series_list_last = MetaData.SeriesCount;
Channel_list = 1: MetaData.SizeC;
Time_list = 1: MetaData.SizeT;
Z_list = 1: MetaData.SizeZ;
if(nargin>=2)
    if(all(varargin{1} == uint16(varargin{1})))
    Series_list_first = varargin{1};
    end
end
if(nargin>=3)
    if(all(varargin{2} == uint16(varargin{2})))
    Series_list_last = varargin{2};
    end
end
if(nargin>=4)
    if(all(varargin{3} == uint16(varargin{3})))
    Channel_list = varargin{3};
    end
end
if(nargin>=5)
    if(all(varargin{4} == uint16(varargin{4})))
    Time_list = varargin{4};
    end
end
if(nargin>=6)
    if(all(varargin{5} == uint16(varargin{5})))
    Z_list = varargin{5};
    end
end



% The main inconvenience of the bfopen.m function is that it loads all the content of an image regardless of its size.
% Initialize BioFormtas Reader
reader = bfGetReader(filename);

% add progress bar
h = waitbar(0,'Processing Data ...');
totalframes = MetaData.SeriesCount * MetaData.SizeC * MetaData.SizeZ * MetaData.SizeT;
framecounter = 0;

% Preallocate array with size (Series, SizeC, SizeZ, SizeT, SizeX, SizeY)
% image6d = zeros(MetaData.SeriesCount, MetaData.SizeT, MetaData.SizeZ, MetaData.SizeC, MetaData.SizeY, MetaData.SizeX);
image6d = zeros(Series_list_last-Series_list_first+1, numel(Time_list), numel(Z_list), numel(Channel_list), MetaData.SizeY, MetaData.SizeX);
Series_list = Series_list_first : Series_list_last ;
% for series = 1: MetaData.SeriesCount
for series = Series_list

    % set reader to current series
    reader.setSeries(series-1);
%     for timepoint = 1: MetaData.SizeT
    for timepoint = Time_list
%         for zplane = 1: MetaData.SizeZ
        for zplane = Z_list
%             for channel = 1: MetaData.SizeC
            for channel = Channel_list

                framecounter = framecounter + 1;
                 % update waitbar
                wstr = {'Reading Images: ', num2str(framecounter), ' of ', num2str(totalframes),'Frames' };
                waitbar(framecounter / totalframes, h, strjoin(wstr))

                % get linear index of the plane (1-based)
                iplane = loci.formats.FormatTools.getIndex(reader, zplane - 1, channel - 1, timepoint -1) +1;
                % get frame for current series
%                 image6d(series, timepoint, zplane, channel, :, :) = bfGetPlane(reader, iplane);
                image6d(find(Series_list == series), find(Time_list == timepoint), find(Z_list == zplane), find(Channel_list == channel), :, :) = bfGetPlane(reader, iplane);

            end
        end
    end
end

% close waitbar
close(h)

% close BioFormats Reader
reader.close();

% store image data and meta information in cell array
out = {};
out{1} = image6d;
out{2} = MetaData;