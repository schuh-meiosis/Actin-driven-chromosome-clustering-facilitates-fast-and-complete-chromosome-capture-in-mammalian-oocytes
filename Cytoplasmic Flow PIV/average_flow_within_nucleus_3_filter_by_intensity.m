%rewriting for better readability and adding averages over intervals
function [angles_all,speeds_filtered,speeds_all,velo_in,velo_side] = average_flow_within_nucleus_3_filter_by_intensity()

    filename_nucleus{1} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_03_LamB+actin\time aligned via membrane channel\L4-5_t1-Split Scenes-03-Time Alignment-01.czi';
    filename_nucleus{2} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_03_LamB+actin\time aligned via membrane channel\L4-5_t1-Split Scenes-04-Time Alignment-02.czi';
    filename_nucleus{3} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_03_LamB+actin\time aligned via membrane channel\L6_t1-Time Alignment-03.czi';
    filename_nucleus{4} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_03_LamB+actin\time aligned via membrane channel\P4_t2-Time Alignment-04.czi';
    filename_nucleus{5} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_03_LamB+actin\time aligned via membrane channel\P6_t2_P7_t1-Split Scenes-01-Time Alignment-05.czi';
    filename_nucleus{6} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_03_LamB+actin\time aligned via membrane channel\P6_t2_P7_t1-Split Scenes-02-Time Alignment-06.czi';
    filename_nucleus{7} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_12_LaminB+actin\time aligned via membrane channel\L1_t1-Time Alignment-07.czi';
    filename_nucleus{8} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_12_LaminB+actin\time aligned via membrane channel\L2_t1-Time Alignment-08.czi';
    filename_nucleus{9} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_26_LaminB+actin\time aligned via membrane channel\L1_t1-Time Alignment-09.czi';
    filename_nucleus{10} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_26_LaminB+actin\time aligned via membrane channel\L7_t1-Time Alignment-10.czi';
    filename_nucleus{11} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_26_LaminB+actin\time aligned via membrane channel\L8_t1-Time Alignment-11.czi';
    filename_nucleus{12} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\2021_11_26_LaminB+actin\time aligned via membrane channel\L9_t2-Time Alignment-01.czi';

    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_03_LamB+actin\L4-5_t1-Split Scenes-03-Time Alignment-01-Create Image Subset-01\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_03_LamB+actin\L4-5_t1-Split Scenes-04-Time Alignment-02-Create Image Subset-02\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_03_LamB+actin\L6_t1-Time Alignment-03-Create Image Subset-03\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_03_LamB+actin\P4_t2-Time Alignment-04-Create Image Subset-04\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_03_LamB+actin\P6_t2_P7_t1-Split Scenes-01-Time Alignment-05-Create Image Subset-05\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_03_LamB+actin\P6_t2_P7_t1-Split Scenes-02-Time Alignment-06-Create Image Subset-06\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_12_LaminB+actin\L1_t1-Time Alignment-07-Create Image Subset-07\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_12_LaminB+actin\L2_t1-Time Alignment-08-Create Image Subset-08\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_26_LaminB+actin\L1_t1-Time Alignment-09-Create Image Subset-09\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_26_LaminB+actin\L7_t1-Time Alignment-10-Create Image Subset-10\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_26_LaminB+actin\L8_t1-Time Alignment-11-Create Image Subset-11\';
    %     settings.folder_vectors = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\settings 3\2021_11_26_LaminB+actin\L9_t1-Time Alignment-12-Create Image Subset-12\';

    folder_vectors{1} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_03_LamB+actin\L4-5_t1-Split Scenes-03-Time Alignment-01-Create Image Subset-01\';
    folder_vectors{2} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_03_LamB+actin\L4-5_t1-Split Scenes-04-Time Alignment-02\';
    folder_vectors{3} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_03_LamB+actin\L6_t1-Time Alignment-03-Create Image Subset-03\';
    folder_vectors{4} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_03_LamB+actin\P4_t2-Time Alignment-04-Create Image Subset-04\';
    folder_vectors{5} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_03_LamB+actin\P6_t2_P7_t1-Split Scenes-01-Time Alignment-05-Create Image Subset-05\';
    folder_vectors{6} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_03_LamB+actin\P6_t2_P7_t1-Split Scenes-02-Time Alignment-06-Create Image Subset-06\';
    folder_vectors{7} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_12_LaminB+actin\L1_t1-Time Alignment-07-Create Image Subset-07\';
    folder_vectors{8} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_12_LaminB+actin\L2_t1-Time Alignment-08-Create Image Subset-08\';
    folder_vectors{9} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_26_LaminB+actin\L1_t1-Time Alignment-09-Create Image Subset-09\';
    folder_vectors{10} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_26_LaminB+actin\L7_t1-Time Alignment-10-Create Image Subset-10\';
    folder_vectors{11} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_26_LaminB+actin\L8_t1-Time Alignment-11-Create Image Subset-11\';
    folder_vectors{12} = 'W:\Eike Urs (emoenni)\Projects\cytoplasmic flow PIV\Ina\results\with PIVlab\2021_11_26_LaminB+actin\L9_t2-Time Alignment-01\';
    
    

    center_chromosomes_manual{1} = [139,125]; %L4-5_t1-Split Scenes-03-Time Alignment-01
    center_chromosomes_manual{2} = [0,0]; %L4-5_t1-Split Scenes-04-Time Alignment-02
    center_chromosomes_manual{3} = [150,154]; %L6_t1-Time Alignment-03-Create Image Subset-03
    center_chromosomes_manual{4} = [162,124]; %P4_t2-Time Alignment-04-Create Image Subset-04
    center_chromosomes_manual{5} = [0,0]; %P6_t2_P7_t1-Split Scenes-01-Time Alignment-05
    center_chromosomes_manual{6} = [147,118]; %P6_t2_P7_t1-Split Scenes-02-Time Alignment-06
    center_chromosomes_manual{7} = [131,156]; %L1_t1-Time Alignment-07-Create Image Subset-07
    center_chromosomes_manual{8} = [113,144]; %L2_t1-Time Alignment-08-Create Image Subset-08
    center_chromosomes_manual{9} = [136,138]; %L1_t1-Time Alignment-09-Create Image Subset-09
    center_chromosomes_manual{10} = [130,124]; %L7_t1-Time Alignment-10-Create Image Subset-10
    center_chromosomes_manual{11} = [118,124]; %L8_t1-Time Alignment-11-Create Image Subset-11
    center_chromosomes_manual{12} = [0,0]; %L9_t2-Time Alignment-01

    angles_all = cell(12,1);
    speeds_all = cell(12,1);
    speeds_filtered = cell(12,1);
    velo_in= cell(12,1);
    velo_side = cell(12,1);
    count_inward = cell(12,1);
    count_outward = cell(12,1);
    count_sideways = cell(12,1);
    count_clockwise = cell(12,1);
    count_counterclockwise = cell(12,1);
    
    for i=10:12
        settings = set_settings(filename_nucleus{i},folder_vectors{i},center_chromosomes_manual{i});
%         settings = set_settings(filename_nucleus{i},folder_vectors{i},[0,0]);

        if i==9 %special settings for L1_t1-Time Alignment-09
            settings.thickness_membrane_um = 4.5;
            settings.local_edgeThreshold = 0.1; 
            settings.local_amount = 0.6;
        end
        [u_interval,v_interval,count_over_time,x_interval,y_interval,u_all,v_all,x_all,y_all,count_over_time_all,image_nucleus_interval,kymograph_speed,kymograph_velo_in,kymograph_velo_side,count_inward{i},count_outward{i},count_clockwise{i},count_counterclockwise{i},count_sideways{i},angles_all{i},speeds_all{i},speeds_filtered{i},velo_in{i},velo_side{i}] = average_flow_within_nucleus_once(settings);
    end
    
    num_max = max(cellfun(@numel,angles_all));
    angles_mat = nan(num_max,12);
    for i=1:12
        angles_mat(1:numel(angles_all{i}),i) = angles_all{i};
    end
    xlswrite('tabletest.xlsx',angles_mat);
end

function [u_interval,v_interval,count_over_time,x_interval,y_interval,u_all,v_all,x_all,y_all,count_over_time_all,image_actin_interval,kymograph_speed,kymograph_velo_in,kymograph_velo_side,count_inward,count_outward,count_clockwise,count_counterclockwise,count_sideways,angles_all,speeds_all,speeds_filtered,velo_in,velo_side] = average_flow_within_nucleus_once(settings)
    colormap('default')
    %addpath('W:\Eike Urs (emoenni)\Projects\MatlabToolboxes\Eike\testing and old stuff')
  
    files_vectors = dir([settings.folder_vectors '*.txt']);
    
    settings.filenames_vectors = sort(string({files_vectors.name}));
    u_all =cell(numel(settings.filenames_vectors),1);
    v_all =cell(numel(settings.filenames_vectors),1);
    x_all =cell(numel(settings.filenames_vectors),1);
    y_all =cell(numel(settings.filenames_vectors),1);
    count_over_time_all =cell(numel(settings.filenames_vectors),1);
    
    u_interval =cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    v_interval =cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    x_interval = cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    y_interval = cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    
    u_interval_filt =cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    v_interval_filt =cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    x_interval_filt = cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    y_interval_filt = cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    speed_interval_filt= cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    
    speed_interval = cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    count_over_time = cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    image_actin_interval = cell(ceil(numel(settings.filenames_vectors)/settings.interval),1);
    
    kymograph_speed   = zeros(settings.num_radii,numel(settings.filenames_vectors));
    kymograph_velo_in = zeros(settings.num_radii,numel(settings.filenames_vectors));
    kymograph_velo_side = zeros(settings.num_radii,numel(settings.filenames_vectors));

    timeagligned_chr_last_frame = get_timeagligned_chr_last_frame(settings,settings.center_chromosomes_manual);
    
    count_inward = 0;
    count_outward = 0;
    count_sideways = 0;
    count_clockwise = 0;
    count_counterclockwise = 0;
    count_total =0;
    
    speeds_all = [];
    speeds_filtered= [];
    velo_in= [];
    velo_side= [];
    angles_all =[];
    
    for time = 1:numel(settings.filenames_vectors)
             
        [u_now,v_now,x_now,y_now,count_now,center_x_now,center_y_now,image_nucleus,area_remove,settings] = get_current_quiver(settings,time);
        
        [segmentation_actin,image_actin] = get_actin_intensity_mask(settings,time);
        
        interval_id = ceil(time/settings.interval);
        
        u_all{time,1} = u_now;
        v_all{time,1} = v_now;
        x_all{time,1} = x_now;
        y_all{time,1} = y_now;
        count_over_time_all{time,1} = count_now;
            
        if isempty(u_interval{interval_id,1})
            u_interval{interval_id,1} = u_now;
            v_interval{interval_id,1} = v_now;
            x_interval{interval_id,1} = x_now;
            y_interval{interval_id,1} = y_now;
            speed_interval{interval_id,1} = sqrt(u_now.^2+v_now.^2);
            count_over_time{interval_id,1} = count_now;
%             image_nucleus_interval{interval_id,1} = image_nucleus;
            image_actin_interval{interval_id,1} = image_actin;
        else
            u_interval{interval_id,1} = u_interval{interval_id,1} + u_now;
            v_interval{interval_id,1} = v_interval{interval_id,1} + v_now;
            x_interval{interval_id,1} = max(x_interval{interval_id,1}, x_now);
            y_interval{interval_id,1} = max(y_interval{interval_id,1}, y_now);
            speed_interval{interval_id,1} = speed_interval{interval_id,1} + sqrt(u_now.^2+v_now.^2);
            count_over_time{interval_id,1} = count_over_time{interval_id,1} + count_now;
%             image_nucleus_interval{interval_id,1} = image_nucleus_interval{interval_id,1} + image_nucleus;
            image_actin_interval{interval_id,1} = image_actin_interval{interval_id,1} + image_actin;
        end
        
        if ~all(settings.center_chromosomes_manual==0)
            center_x_now = timeagligned_chr_last_frame(1);
            center_y_now = timeagligned_chr_last_frame(2);
        end
        
        x_now(x_now==0)=nan;
        y_now(y_now==0)=nan;
        distances_now = sqrt((x_now - center_x_now).^2+(y_now - center_y_now).^2);
        speeds_now = sqrt((u_now).^2+(v_now).^2);
        inward_vector_x = -1*(x_now - center_x_now);
        inward_vector_y = -1*(y_now - center_y_now);
        inward_length = sqrt(inward_vector_x.^2+inward_vector_y.^2);
        inward_vector_x = inward_vector_x./inward_length; %normalize
        inward_vector_y = inward_vector_y./inward_length;
        sideways_vector_x = inward_vector_y;
        sideways_vector_y = -1*inward_vector_x;
        inward_velocity = sum(cat(3,u_now .* inward_vector_x, v_now .* inward_vector_y),3);
        sideways_velocity = sum(cat(3,u_now .* sideways_vector_x, v_now .* sideways_vector_y),3);
        for rad = 1:settings.num_radii
            
            
            ids_rad = (distances_now > (rad-1)*settings.dis_radii) & (distances_now <= (rad)*settings.dis_radii);
%             kymograph_speed(rad,time) = mean(nonzeros(speeds_now(ids_rad)),'omitnan');
%             kymograph_velo_in(rad,time) = mean(nonzeros(inward_velocity(ids_rad)),'omitnan');
%             kymograph_velo_side(rad,time) = mean(nonzeros(sideways_velocity(ids_rad)),'omitnan');
            
            kymograph_speed(rad,time) = mean((speeds_now(ids_rad)),'omitnan');
            kymograph_velo_in(rad,time) = mean((inward_velocity(ids_rad)),'omitnan');
            kymograph_velo_side(rad,time) = mean((sideways_velocity(ids_rad)),'omitnan');
        end
%         colormap(gray)   
%         daspect([1 1 1]);
%         drawnow
        
%         filename_png = [filename_vector_now '.png'];
%         saveas(ax_nucleus,filename_png,'tiffn');
%         print(ax_nucleus,filename_png,'-dpng','-r300');
%         export_fig(filename_png ,'-m2.5')
%         close all
%         thresh_too_slow = speeds_now>2;
        thresh_no_actin = logical(segmentation_actin) & ~isnan(speeds_now);
        
        id_in = abs(inward_velocity)>abs(sideways_velocity) & inward_velocity>0;
        id_out = abs(inward_velocity)>abs(sideways_velocity) & inward_velocity<0;
        id_side = abs(inward_velocity)<abs(sideways_velocity) ;
        id_clockwise = abs(inward_velocity)<abs(sideways_velocity) & sideways_velocity>0;
        id_counterclockwise = abs(inward_velocity)<abs(sideways_velocity) & sideways_velocity<0;
        
        [theta_now,rho] = cart2pol(inward_velocity,sideways_velocity);
        
        count_inward = count_inward + sum(id_in(thresh_no_actin));
        count_outward = count_outward + sum(id_out(thresh_no_actin));
        count_sideways = count_sideways + sum(id_side(thresh_no_actin));
        count_clockwise = count_clockwise + sum(id_clockwise(thresh_no_actin));
        count_counterclockwise = count_counterclockwise + sum(id_counterclockwise(thresh_no_actin));
        count_total = count_total + numel(nonzeros(speeds_now(:)));
        
        speeds_now_filt = speeds_now(thresh_no_actin);
        theta_now_filt = theta_now(thresh_no_actin);
        inward_velocity_now = inward_velocity(thresh_no_actin);
        sideways_velocity_now = sideways_velocity(thresh_no_actin);
        
        speeds_all = [speeds_all; speeds_now(:)];
        speeds_filtered = [speeds_filtered; speeds_now_filt(:)];
        velo_in= [velo_in; inward_velocity_now(:)];
        velo_side= [velo_side; sideways_velocity_now(:)];
        angles_all = [angles_all; theta_now_filt(:)];
        
        if isempty(u_interval_filt{interval_id,1})
            u_interval_filt{interval_id,1} = u_now .* thresh_no_actin;
            v_interval_filt{interval_id,1} = v_now .* thresh_no_actin;
            x_interval_filt{interval_id,1} = x_now .* thresh_no_actin;
            y_interval_filt{interval_id,1} = y_now .* thresh_no_actin;
            speed_interval_filt{interval_id,1} = sqrt(u_now.^2+v_now.^2)  .* thresh_no_actin;
        else
            u_interval_filt{interval_id,1} = u_interval_filt{interval_id,1} + u_now.* thresh_no_actin;
            v_interval_filt{interval_id,1} = v_interval_filt{interval_id,1} + v_now.* thresh_no_actin;
            x_interval_filt{interval_id,1} = max(x_interval_filt{interval_id,1}, x_now.* thresh_no_actin);
            y_interval_filt{interval_id,1} = max(y_interval_filt{interval_id,1}, y_now.* thresh_no_actin);
            speed_interval_filt{interval_id,1} = speed_interval_filt{interval_id,1} + sqrt(u_now.^2+v_now.^2).* thresh_no_actin;
        end

    end
    
    create_images(kymograph_speed,kymograph_velo_in,kymograph_velo_side,u_interval_filt,v_interval_filt,x_interval_filt,y_interval_filt,speed_interval_filt,count_over_time,image_actin_interval,settings)
end


function [segmentation,membrane_bwdist] = fit_membrane(image_membrane_BW,center_nuc,settings)
    [x,y] = ind2sub(size(image_membrane_BW),find(image_membrane_BW)); 
    x = x - center_nuc(1);
    y = y - center_nuc(2);
    [theta,rho] = cart2pol(x,y);

    [theta_sorted,index_sort] = sort(theta);
    rho_sorted = rho(index_sort);

    [C,ia,ic] = unique(theta_sorted);
    theta_sorted_uniqued = C; 
    rho_sorted_uniqued = rho_sorted(ia);

    [B,TF] = rmoutliers(rho_sorted_uniqued,'movmedian',0.5,'SamplePoints',theta_sorted_uniqued,'ThresholdFactor',1);
%         f = fit(theta,rho,'smoothingspline','SmoothingParam',0.9995);
    f = fit(theta_sorted_uniqued(~TF),rho_sorted_uniqued(~TF),'smoothingspline','SmoothingParam',0.9995);
%     plot(f,theta,rho);
    % evaluate
    %f(3)%as simple as that

    angles = (0:(settings.num_steps-1))'*2*pi/settings.num_steps -pi;
    rhos = f(angles);
    [x_fit,y_fit] = pol2cart(angles,rhos);
    x_fit = round(x_fit + center_nuc(1));
    y_fit = round(y_fit + center_nuc(2));
    BW_skeleton = zeros(size(image_membrane_BW));
    for i=1:numel(x_fit)
        BW_skeleton(x_fit(i),y_fit(i)) = 1;
    end

    membrane_bwdist = bwdist(BW_skeleton);
    segmentation = membrane_bwdist < settings.thickness_membrane;
    
    
end

function settings = set_settings(filename,folder_vectors,center_chromosomes_manual)

    settings.center_chromosomes_manual = center_chromosomes_manual;

    settings.thickness_membrane_um = 2.5; %general setting
%     settings.thickness_membrane_um = 4.5; %special setting for L1_t1-Time Alignment-09
    settings.thickness_outer_um = 5;
    settings.thickness_inner_um = 5;

    %settings for local contrast filter when presegmenting membrane
    settings.local_edgeThreshold = 0.3;
    settings.local_amount = 0.5;
%     settings.local_edgeThreshold = 0.1; %special settings for L1_t1-Time Alignment-09
%     settings.local_amount = 0.6;

    settings.local_edgeThreshold_actin =0.1;
    settings.local_amount_actin = 0.3;
    
    
    settings.threshold_bw_actin = 0.05; %fraction of segmented actin in radius of vector to segment vectors
    
    settings.channel_membrane =1;
    settings.channel_actin=2;

    settings.prctile_use = 99;
    settings.number_cutouts = 10;
    settings.num_steps = 800; %determines the angles at which to probe the smoothing spline to create the membrane segmentation 
   
    settings.color_scale = 1;
    settings.color_value_max = 10;
    settings.color_value_min = 0;
    settings.length_scale = 1;
    
    settings.kymo_speed_max = 10;
    settings.kymo_velo_max = 10;
    
    settings.cmap = colormap('jet');
    
    settings.interval = 5;
    
    settings.num_radii = 10;
    
    settings.dis_radii = 10;%distance in pixels, same unit as in raw data of input files
    
    settings.filename = filename;
    settings.folder_vectors = folder_vectors;
    
    [filepath,name,ext] = fileparts(filename);
    settings.filename_short = name;
end

function [u_now,v_now,x_now,y_now,count_now,center_x,center_y,image_nucleus,area_remove,settings] = get_current_quiver(settings,time)    
    
    data_membrane = ReadImage6D3(settings.filename,1,1,settings.channel_membrane,time);
    settings.pixelsize_x = data_membrane{2}.ScaleX;
    
    

    filename_vector_now = [char(settings.folder_vectors) char(settings.filenames_vectors(time))];
    M = dlmread(filename_vector_now,' ');
    x = M(:,1);
    y = M(:,2);
    u = M(:,3);
    v = M(:,4);
    
%     uandv = [u;v];
%     u = rand(size(u))*(max(uandv(:))-min(uandv(:)))+min(uandv(:));
%     v = rand(size(u))*(max(uandv(:))-min(uandv(:)))+min(uandv(:));
%     I = sqrt(u.^2+v.^2);

    [segmentation,image_nucleus,area_remove,center_x,center_y] = segment_membrane(data_membrane,settings);

%     c = colormap(jet);
%     c_num = size(c,1);
%     Icolor_idx = round(I*settings.color_scale/min_difference*(c_num-1))+1;
%     Icolor_idx = round((I-settings.color_value_min)/(settings.color_value_max-settings.color_value_min)*(c_num-1))+1;
% 
%     Icolor_idx(Icolor_idx>c_num) = c_num;
%     Icolor_idx(Icolor_idx<1) = 1;
%     Icolor = c(Icolor_idx,:);

    x_pxl = x;
    y_pxl = y;

    [x_area_remove,y_area_remove] = ind2sub(size(area_remove),find(area_remove));
    ids_remove = ismember([x y],[y_area_remove(:) x_area_remove(:)],'rows');
    x_pxl(ids_remove,:) = [];
    y_pxl(ids_remove,:) = [];
    u_pxl = u*settings.length_scale;
    v_pxl = v*settings.length_scale;
    u_pxl(ids_remove,:) = [];
    v_pxl(ids_remove,:) = [];
%     Icolor(ids_remove,:) = [];

%     q = quiver(x_pxl(:,1),y_pxl(:,1),u_pxl(:),v_pxl(:),1);
%     cquiver(x_pxl(:,1),y_pxl(:,1),u_pxl(:),v_pxl(:));
%     quiverC2D(x_pxl(:,1),y_pxl(:,1),u_pxl(:),v_pxl(:));
%     q.LineWidth = 2;
%     q.MaxHeadSize = 30;    

    u_now = zeros(size(segmentation));
    v_now = zeros(size(segmentation));
    x_now = zeros(size(segmentation));
    y_now = zeros(size(segmentation));
    count_now = zeros(size(segmentation));
    
    for ii = 1:size(x_pxl,1)

        x_pxl_here = x_pxl(ii,:);
        y_pxl_here = y_pxl(ii,:);
        u_pxl_here = u_pxl(ii);
        v_pxl_here = v_pxl(ii);

%         q = quiver(x_pxl_here(1),y_pxl_here(1),u_pxl_here,v_pxl_here,30,'filled','color',Icolor(ii,:),'MaxHeadSize',20);
        if ~isnan(u_pxl_here ) && ~isnan(v_pxl_here )
%             u_now(x_pxl_here,y_pxl_here) = u_now(x_pxl_here,y_pxl_here) + u_pxl_here;
%             v_now(x_pxl_here,y_pxl_here) = v_now(x_pxl_here,y_pxl_here) + v_pxl_here;
            
            u_now(x_pxl_here,y_pxl_here) = u_pxl_here;
            v_now(x_pxl_here,y_pxl_here) = v_pxl_here;
        end
        x_now(x_pxl_here,y_pxl_here) = x_pxl_here;
        y_now(x_pxl_here,y_pxl_here) = y_pxl_here;
        count_now(x_pxl_here,y_pxl_here) = count_now(x_pxl_here,y_pxl_here) + 1;
    end 
end

function [segmentation,image_nucleus,area_remove,center_x,center_y] = segment_membrane(data_membrane,settings)
    image_nucleus = squeeze(data_membrane{1}); 
    
    settings.thickness_membrane = round(settings.thickness_membrane_um/settings.pixelsize_x);
%     BW = edge(localcontrast(uint16(image_nucleus),0.3,0.5),'Canny');%     %general setting
%     BW = edge(localcontrast(uint16(image_nucleus),0.1,0.6),'Canny'); %special settings for  2021_11_26_LaminB+actin / L1_t1-Time Alignment-09
    BW = edge(localcontrast(uint16(image_nucleus),settings.local_edgeThreshold,settings.local_amount),'Canny'); 
    [x_bw,y_bw] = ind2sub(size(BW),find(BW));
    center_nuc = [mean(x_bw) mean(y_bw)];
    segmentation = fit_membrane(BW,center_nuc,settings);
    [x_2,y_2] = ind2sub(size(segmentation),find(segmentation));
    center_nuc_2 = [mean(x_2) mean(y_2)];
    [segmentation,membrane_bwdist] = fit_membrane(BW,center_nuc_2,settings);   
    areas_nonmembrane = bwareafilt(logical(1-segmentation),[20 numel(segmentation)]);
    areas_nonmembrane = bwareafilt(logical(1-areas_nonmembrane),1); %keep only the largest CC
    areas_nonmembrane = bwlabeln(1-areas_nonmembrane);
    bw_middle = zeros(size(segmentation));
    bw_middle(round(size(segmentation,1)/2),round(size(segmentation,2)/2))=1;
    distance_middle = bwdist(bw_middle);
    distance_avg_1 = mean(distance_middle(areas_nonmembrane==1));
    distance_avg_2 = mean(distance_middle(areas_nonmembrane==2));
    if distance_avg_1<distance_avg_2
        area_remove = areas_nonmembrane~=1;

    else
        area_remove = areas_nonmembrane~=2;
    end
    
    center_x = center_nuc_2(1);
    center_y = center_nuc_2(2);
end

function create_images(kymograph_speed,kymograph_velo_in,kymograph_velo_side,u_interval,v_interval,x_interval,y_interval,speed_interval,count_over_time,image_nucleus_interval,settings)
    
    kymograph_speed(isnan(kymograph_speed)) = 0;
    kymograph_velo_in(isnan(kymograph_velo_in)) = 0;
    kymograph_velo_side(isnan(kymograph_velo_side)) = 0;
    
    yticks_here = string((settings.num_radii:-1:1)*settings.pixelsize_x*settings.dis_radii);
    
    speed_max = ceil(max(abs(kymograph_speed(:))));
    velo_in_max = ceil(max(abs(kymograph_velo_in(:))));
    velo_side_max = ceil(max(abs(kymograph_velo_side(:))));
    
    figure('units','normalized','outerposition',[0 0 1 1])
    imagesc(kymograph_speed(end:-1:1,:))
%     caxis([0 settings.kymo_speed_max])
    caxis([0 speed_max]);
    set(gca,'fontsize',40)
    colormap(jet)
    cbar = colorbar;
    yticklabels(yticks_here)
    yticks(1:10)
    xlabel('frame')
    ylabel('radius[um]')
    title('Kymograph Speed')
    cbar.Label.String = 'Mean Speed [pixel/frame]';
    saveas(gcf,[settings.filename_short '_Kymograph_Speed.png'],'png');
    
    imagesc(kymograph_velo_in(end:-1:1,:))
%     caxis([-settings.kymo_velo_max settings.kymo_velo_max])
    caxis([-velo_in_max velo_in_max]);
    set(gca,'fontsize',40)
    colormap(jet)
    cbar = colorbar;
    yticklabels(yticks_here)
    yticks(1:10)
    xlabel('frame')
    ylabel('radius[um]')
    title('Kymograph Velocity center')
    cbar.Label.String = 'Velocity center [pixel/frame]';
    saveas(gcf,[settings.filename_short '_Kymograph_Velo_center.png'],'png');
    
    imagesc(kymograph_velo_side(end:-1:1,:))
%     caxis([-settings.kymo_velo_max settings.kymo_velo_max]);
    caxis([-velo_side_max velo_side_max]);
    set(gca,'fontsize',40)
    colormap(jet)
    cbar = colorbar;
    yticklabels(yticks_here)
    yticks(1:10)
    xlabel('frame')
    ylabel('radius[um]')
    title('Kymograph Velocity circular')
    cbar.Label.String = 'Velocity circular [pixel/frame]';
    saveas(gcf,[settings.filename_short '_Kymograph_Velo_circ.png'],'png');
    
    num_intervals = size(image_nucleus_interval,1);
    
    
    
    
    for interval =1:num_intervals
        
        u_interval{interval} = u_interval{interval};% ./ count_over_time{interval} * settings.interval;
        v_interval{interval} = v_interval{interval}; %./ count_over_time{interval} * settings.interval;
        speed_interval{interval} = speed_interval{interval};% ./ count_over_time{interval};    
%         
    end
    
    speed_interval_max = ceil(max(cellfun(@(x) max(x(:)),speed_interval)));
    
    for interval =1:num_intervals
        
        filename_png = [settings.filename_short '_interval ' num2str(interval) '.png'];
%         imagesc(image_nucleus_interval{interval});
        ids_nonzero = u_interval{interval} > 0 & v_interval{interval}>0;
        
        image_c = prepare_image(image_nucleus_interval{interval},settings);
        image(image_c);
        xticks([]);
        yticks([]);
        daspect([1 1 1]);
%         caxis([settings.color_value_min settings.color_value_max]);
        caxis([0 speed_interval_max]);
        colormap('gray');
        hold on
%         quiver(x_interval{interval}(:),y_interval{interval}(:),u_interval{interval}(:),v_interval{interval}(:),10,'MaxHeadSize',20);
        quiver_custom(x_interval{interval}(ids_nonzero),y_interval{interval}(ids_nonzero),u_interval{interval}(ids_nonzero),v_interval{interval}(ids_nonzero),speed_interval{interval}(ids_nonzero),settings,speed_interval_max);
        quiv = gcf;
        hold off
        
        colormap(settings.cmap);
        cbar = colorbar;
        cbar.Label.String = 'Total Displacement over entire interval [pixel]';
        set(gca,'fontsize',40)
        saveas(gcf,filename_png,'png');
    end
end

function quiv = quiver_custom(x,y,u,v,color_val,settings,speed_interval_max)
    hold on
    num_val = numel(x);
    
    c_num = size(settings.cmap,1);
    for i=1:num_val
        if (abs(u(i)) + abs(v(i)))>0
%             Icolor_idx = round((color_val(i)-settings.color_value_min)/(settings.color_value_max-settings.color_value_min)*(c_num-1))+1;
            Icolor_idx = round((color_val(i)-0)/(speed_interval_max-0)*(c_num-1))+1;
            Icolor_idx(Icolor_idx>c_num) = c_num;
            Icolor_idx(Icolor_idx<1) = 1;
            Icolor = settings.cmap(Icolor_idx,:);

%             quiv = quiver(x(i),y(i),u(i),v(i),1/settings.interval,'MaxHeadSize',20); %divide length by interval to get per frame movement
            quiv = quiver(x(i),y(i),u(i),v(i),1,'MaxHeadSize',20); %divide length by interval to get per frame movement
            quiv.Color = Icolor;
            
        end
    end
    
    hold off
end

function image_c = prepare_image(image_nucleus_interval,settings)
    %add white 10 um bar
    limits_x(1) = round(0.9*size(image_nucleus_interval,1));
    limits_x(2) = round(0.9*size(image_nucleus_interval,1)+0.5/settings.pixelsize_x);
    limits_y(1) = round(0.05*size(image_nucleus_interval,1));
    limits_y(2) = round(0.05*size(image_nucleus_interval,1)+10/settings.pixelsize_x);
    image_nucleus_interval(limits_x(1):limits_x(2),limits_y(1):limits_y(2)) = max(image_nucleus_interval(:));
    %use 3 color image to decouple colorbar later on
    image_c = repmat(image_nucleus_interval, [1 1 3]);
    image_c = uint8((image_c-min(image_c(:))) / max(image_c(:)-min(image_c(:))) * 256);
    
end

function timeagligned_chr_last_frame = get_timeagligned_chr_last_frame(settings,center_chromosomes_manual)
    time_last = numel(settings.filenames_vectors)+1; %there is 1 more image than txt file with movement between images
    data_membrane = ReadImage6D3(settings.filename,1,1,settings.channel_membrane,time_last); 
    image_nucleus = squeeze(data_membrane{1}); 
    min_x = find(sum(abs(image_nucleus),1)==0,1,'first');
    max_x = find(sum(abs(image_nucleus),1)==0,1,'last');
    min_y = find(sum(abs(image_nucleus),2)==0,1,'first');
    max_y = find(sum(abs(image_nucleus),2)==0,1,'last');
    timeagligned_x = center_chromosomes_manual(1);
    timeagligned_y = center_chromosomes_manual(2);
    if ~isempty(min_x)
        if min_x == 1
            timeagligned_x = center_chromosomes_manual(1)+max_x;
        else
            timeagligned_x = center_chromosomes_manual(1)+min_x-max_x-1;
        end
    end
    if ~isempty(min_y)
        if min_y == 1
            timeagligned_y = center_chromosomes_manual(2)+max_y;
        else
            timeagligned_y = center_chromosomes_manual(2)+min_y-max_y-1;
        end
    end
    timeagligned_chr_last_frame = [timeagligned_x,timeagligned_y];
end

function [segmentation_actin,image_actin] = get_actin_intensity_mask(settings,time)
    data_actin = ReadImage6D3(settings.filename,1,1,settings.channel_actin,time);
    image_actin = squeeze(data_actin{1});
    
%     BW_actin = edge(localcontrast(uint16(image_actin),settings.local_edgeThreshold_actin,settings.local_amount_actin ),'Canny'); 
%     edge(localcontrast(uint16(image_actin),0.1,0.3),'Canny')
%     BW_actin = edge(locallapfilt(single(image_actin)/max(image_actin(:)),0.4,0.9),'Sobel',0.1);
    BW_actin = edge(imdiffusefilt(uint8(image_actin)),'Canny');
    
    filename_vector_now = [char(settings.folder_vectors) char(settings.filenames_vectors(time))];
    M = dlmread(filename_vector_now,' ');
    x = M(:,1);
    y = M(:,2);
    u = M(:,3);
    v = M(:,4);
    
    distances = pdist(x);
    distance_smallest = min(distances(distances>0));
    radius = floor(distance_smallest/4); %try not to touch the membrane
    
%     se = strel('disk',radius);
    segmentation_actin = zeros(size(image_actin));
    
    for ii=1:size(x,1)
        area_check = BW_actin(x(ii)-radius:x(ii)+radius,y(ii)-radius:y(ii)+radius);
        is_actin = mean(area_check(:))>= settings.threshold_bw_actin ;
        segmentation_actin(x(ii),y(ii)) = is_actin;
    end
end