
# Average Flow within Nucleus_3

---

##Overview:
	This code was specifically written to process output from the Matlab software PIVlab
	https://de.mathworks.com/matlabcentral/fileexchange/27659-pivlab-particle-image-velocimetry-piv-tool-with-gui
	This information is combined with segmentations of the nuclear membrane and the manually assessed centers of the nucleus to determine the average flow direction.
	This script is not directly usable for analysis of new data and is only published for transparency.
	
---
	
##Installation

	Extract the folder to any location.
	Navigate to the extracted folder in Matlab.
	Add Bioformats via path variable into Matlab
	
---

##Requirements
### Matlab
		Tested on Matlab R2018b in Windows 10
		'Image Processing Toolbox'
		'Statistics and Machine Learning Toolbox'
		'Curve Fitting Toolbox'

### Bioformats for Matlab
		Tested with Version 6.7
	https://downloads.openmicroscopy.org/bio-formats/
---

##Description
	PIVlab was used without any masking.
	The nuclear membrane is segmented in order to ignore any flows detected outside the nucleus.
	This segmentation is performed by fitting a smoothing spline to the radial and angular coordinates of a preliminary segmentation of the membrane.
	The preliminary segmentation is performed by using the Matlab function localcontrast.m and edge.m with the "Canny" method.
	The center of the nucleus was determined by eye within Zeiss Zen software and was primarily based on the chromosome channel.
	
		
## Usage
	use settings for PIVlab as saved in PIVlab_set_emoenni.mat
	export data into text files
	determine center of chromosome mass in Zen and note x and y coordinates
	enter center of chromosomes mass coordinates in script
	enter text files and original czi files in script
	run script within matlab
	

###		Tipps:
	
---

## Troubleshooting:
	
---
	
##Contact
	eike-urs.moennich@mpinat.mpg.de
---
